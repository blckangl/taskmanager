import { User } from './../../models/User';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
// Do not import from 'firebase' as you'll lose the tree shaking benefits
import * as firebase from 'firebase/app';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthServiceProvider {

  private user: Observable<firebase.User>;
  constructor(public afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
  }

  get authenticated(): boolean {
    return this.getUser() !== null;
  }

 login() {
  this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
}
loginUser(email: string, password: string):firebase.Promise<any> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
}
singUp(email: string, password: string):firebase.Promise<any> {
 return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
}

getUser(){
  return this.afAuth.auth.currentUser;
}
logout() {
  this.afAuth.auth.signOut();
  console.log(this.getUser());
}
buildUser():User{
  let user = new User();
user.email = this.afAuth.auth.currentUser.email;
user.Id = this.afAuth.auth.currentUser.uid;
user.NickName="";

  return user;
}

}