import { Task } from './Task';
export class Team{
    Id : string;
    Name:string;
    Tasks : Task[];
    Archive : Task[];
    UserId : string[];
    Creator:string;
    constructor(){
        this.UserId = [];
        this.Tasks = [];
        this.Archive = [];
    }
}