export class Chat{
    id:string;
  date:Date;
  senderId:string;
  senderName:string;
  content:string;
}