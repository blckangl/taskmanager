import { TaskCreationPage } from './../pages/task-creation/task-creation';
import { TeamJoinPage } from './../pages/team-join/team-join';
import { TeamViewPage } from './../pages/team-view/team-view';
import { TeamCreationPage } from './../pages/team-creation/team-creation';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import {TeamArchivePage} from './../pages/team-archive/team-archive';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import * as Clip from 'clipboard/dist/clipboard.min.js';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
export const firebaseConfig = {
 apiKey: "AIzaSyAx7PjIDk_l5jB7yX0SYzAlUrbfm5L7rGo",
    authDomain: "taskmanager-edd4e.firebaseapp.com",
    databaseURL: "https://taskmanager-edd4e.firebaseio.com",
    projectId: "taskmanager-edd4e",
    storageBucket: "taskmanager-edd4e.appspot.com",
    messagingSenderId: "338450872539"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage
    ,
    LoginPage,
    RegisterPage,
    TeamCreationPage,
    TeamViewPage,
    TeamJoinPage,
    TaskCreationPage,
    TeamArchivePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule ,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    TeamCreationPage,
    TeamViewPage,
    TeamJoinPage,
    TaskCreationPage,
    TeamArchivePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider
  ]
})
export class AppModule {}
