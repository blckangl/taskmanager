import { User } from './../../models/User';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,Loading,LoadingController,ToastController } from 'ionic-angular';
import { AngularFireDatabase} from 'angularfire2/database';
/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user:User;
loading: Loading;
  registerCredentials = { email: '', password: '',NickName:'' };
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private auth: AuthServiceProvider, 
  private alertCtrl: AlertController, 
  private loadingCtrl: LoadingController,
   public toastCtrl: ToastController,
  private db: AngularFireDatabase) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }


  SignUp(){
    if(this.registerCredentials.NickName.length<4)
    {
       this.showMsg("invalid name") ;
        return;
    }
   else{ this.auth.singUp(this.registerCredentials.email,this.registerCredentials.password)
    .then(success=>{
      this.user = new User();
      this.user = this.auth.buildUser();
      this.user.NickName=this.registerCredentials.NickName;
    this.db.object("/Users/"+this.user.Id).set(this.user);
           this.navCtrl.pop();
    }).catch(()=>
      this.showError("Problem creating account.")
    );}
   
  }
 showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
   showError(text) {
     if(this.loading)
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
  showMsg(msg:string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }
}
