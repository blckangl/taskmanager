import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamJoinPage } from './team-join';

@NgModule({
  declarations: [
    TeamJoinPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamJoinPage),
  ],
  exports: [
    TeamJoinPage
  ]
})
export class TeamJoinPageModule {}
