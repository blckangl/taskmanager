import { Team } from './../../models/Team';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';

import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { AngularFireDatabase, FirebaseObjectObservable  } from 'angularfire2/database';
/**
 * Generated class for the TeamJoinPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-team-join',
  templateUrl: 'team-join.html',
})
export class TeamJoinPage {
private id:string;
team:Team;
teamitem:FirebaseObjectObservable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private db: AngularFireDatabase,
   private auth: AuthServiceProvider,
   public toastCtrl:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamJoinPage');
  }
joinTeam(){
  var exist = false;
  console.log(this.id);
this.teamitem =this.db.object("/Teams/"+this.id,{preserveSnapshot:true});
console.log(this.teamitem);
this.teamitem.subscribe(snapshots=>{
  if(snapshots.val()){
    this.team = new Team();
    this.team=snapshots.val();
  }
  else
  {this.team=null;
    this.showMsg("Team Not found");
  return;

}
})
console.log("***team");
console.log(this.team);
if(this.team.Id){
  if(this.team.UserId){
  
  this.team.UserId.forEach(x=>{
    if(x === this.auth.getUser().uid){
      exist=true;
    }
  })

}else{
  this.team.UserId = [];
}

if(!exist){
    this.team.UserId.push(this.auth.getUser().uid);
    this.teamitem.update(this.team);
    this.db.object("Users/"+this.auth.getUser().uid+"/TeamId").set(this.team.Id);
    this.navCtrl.pop();
  }
  else{

   this.showMsg("You are already in this team");
  }


}


}
showMsg(msg:string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }
}
