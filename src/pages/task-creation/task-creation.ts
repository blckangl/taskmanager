import { Task } from './../../models/Task';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable,FirebaseListObservable } from 'angularfire2/database';
/**
 * Generated class for the TaskCreationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-task-creation',
  templateUrl: 'task-creation.html',
})
export class TaskCreationPage {
task:Task;
loading: Loading;
  user: string = "";
  users :string[];
  urgent : false;
  userObject : FirebaseObjectObservable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private loadingCtrl:LoadingController,
  private db: AngularFireDatabase) {
    this.task = new Task();
    this.task.creatorId = navParams.get("id");
    this.task.IsDone = false;
    this.task.UserId=navParams.get("id");
    this.task.creatorName = navParams.get("username");
    this.task.IsTeamTask=true;

    this.task.date = new Date();
   this.userObject = this.db.object("Teams/"+this.navParams.get("teamid")+"/UserId",{preserveSnapshot:true});
   this.userObject.subscribe(snapshots=>{
     this.users = snapshots.val();
     console.log(this.users);
   })
  }

  AddTask(){
    if(this.task.IsTeamTask){
       let teamid:string = this.navParams.get("teamid");
       if(teamid)
       if(teamid.length >4){
         if(this.urgent){
 this.showLoading();
         this.task.ReceiverId = this.user;
         this.task.ReceiverName = this.getUserName(this.task.ReceiverId);
         this.task.Id = this.db.list("Teams/"+teamid+"/Tasks/Urgent").push(this.task).key;
         this.db.object("Teams/"+teamid+"/Tasks/Urgent/"+this.task.Id).update(this.task).then(success=>{
  this.navCtrl.pop();
         });
         }
         else{
 this.showLoading();
         this.task.ReceiverId = this.user;
         this.task.ReceiverName = this.getUserName(this.task.ReceiverId);
         this.task.Id = this.db.list("Teams/"+teamid+"/Tasks/Normal").push(this.task).key;
         this.db.object("Teams/"+teamid+"/Tasks/Normal/"+this.task.Id).update(this.task).then(success=>{
  this.navCtrl.pop();
         });
         }
        
      
       }
    }
    else{
 this.showLoading();
 this.task.Id=  this.db.list("Users/"+this.task.UserId+"/Task/").push(this.task).key;
      this.db.object("Users/"+this.task.UserId+"/Task/"+this.task.Id).update(this.task).then(success=>{
        this.navCtrl.pop();
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskCreationPage');
  }
   showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 getUserName(id:string):string{
   let name =""; 
this.db.object("Users/"+id+"/NickName",{preserveSnapshot:true}).subscribe(snapshtos=>{
  name = snapshtos.val();
})

return name;
}
}
