import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamArchivePage } from './team-archive';

@NgModule({
  declarations: [
    TeamArchivePage,
  ],
  imports: [
    IonicPageModule.forChild(TeamArchivePage),
  ],
  exports: [
    TeamArchivePage
  ]
})
export class TeamArchivePageModule {}
