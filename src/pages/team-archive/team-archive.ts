import { Task } from './../../models/Task';
import { TaskCreationPage } from './../task-creation/task-creation';
import { User } from './../../models/User';
import { LoginPage } from './../login/login';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable,FirebaseListObservable } from 'angularfire2/database';
/**
 * Generated class for the TeamArchivePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-team-archive',
  templateUrl: 'team-archive.html',
})
export class TeamArchivePage {
public user :User;
private userItem :FirebaseObjectObservable<any>;
private userTeamId:FirebaseObjectObservable<any>;
private MyTasksItem :FirebaseListObservable<any>;
private TeamTasks:FirebaseListObservable<any>;
teamId:string;
  constructor(public navCtrl: NavController, private auth:AuthServiceProvider,private db: AngularFireDatabase,private alertCtrl: AlertController) {

   this.userItem = this.db.object("Users/"+this.auth.getUser().uid,{preserveSnapshot:true});
   this.userItem.subscribe(snapshots=>{
     this.user = new User();
     this.user.Id=this.auth.getUser().uid;
     this.user.NickName = snapshots.val()["NickName"];
     
     this.user.TeamId =  snapshots.val()["TeamId"];
    this.MyTasksItem = this.db.list("/Users/"+this.user.Id+"/Task");
  
   })
  
  this.userTeamId = this.db.object("Users/"+this.auth.getUser().uid+"/TeamId",{preserveSnapshot:true});

  this.userTeamId.subscribe(snapshots=>{
    this.teamId= snapshots.val();
    this.TeamTasks = this.db.list("/Teams/"+this.teamId+"/Archive");
  })
    
    
}
ToggleChangeTeam(item:Task){
  this.db.object("/Teams/"+this.user.TeamId+"/Archive/"+item.Id+"/IsDone").set(item.IsDone);
  console.log("/Teams/"+this.user.TeamId+"/Archive/"+item.Id+"/IsDone");
  // console.log("toggled");
  //  let alert = this.alertCtrl.create({
  //   title: 'Low battery',
  //   subTitle: '10% of battery remaining',
  //   buttons: ['Dismiss']
  // });
  // alert.present();
}
  }
