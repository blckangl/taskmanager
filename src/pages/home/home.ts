import { Task } from './../../models/Task';
import { TaskCreationPage } from './../task-creation/task-creation';
import { User } from './../../models/User';
import { LoginPage } from './../login/login';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable,FirebaseListObservable } from 'angularfire2/database';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
public user :User;
private userItem :FirebaseObjectObservable<any>;
private userTeamId:FirebaseObjectObservable<any>;
private MyTasksItem :FirebaseListObservable<any>;
private TeamTasks:FirebaseListObservable<any>;
private UrgentTasks:FirebaseListObservable<any>;
teamId:string;
  constructor(public navCtrl: NavController, private auth:AuthServiceProvider,private db: AngularFireDatabase,private alertCtrl: AlertController) {

   this.userItem = this.db.object("Users/"+this.auth.getUser().uid,{preserveSnapshot:true});
   this.userItem.subscribe(snapshots=>{
     this.user = new User();
     this.user.Id=this.auth.getUser().uid;
     this.user.NickName = snapshots.val()["NickName"];
     
     this.user.TeamId =  snapshots.val()["TeamId"];
    this.MyTasksItem = this.db.list("/Users/"+this.user.Id+"/Task");
   
  
   })
  
  this.userTeamId = this.db.object("Users/"+this.auth.getUser().uid+"/TeamId",{preserveSnapshot:true});

  this.userTeamId.subscribe(snapshots=>{
    this.teamId= snapshots.val();
    this.TeamTasks = this.db.list("/Teams/"+this.teamId+"/Tasks/Normal");
     this.UrgentTasks = this.db.list("/Teams/"+this.teamId+"/Tasks/Urgent");
  })
    
    
   
   
  }
 
signOut(){
    this.userItem = null;
   this.userTeamId = null;
   this.MyTasksItem = null;
   this.TeamTasks = null;
  this.auth.logout();
   this.navCtrl.setRoot(LoginPage);
 
}
public RemoveItem(item){
  this.MyTasksItem.remove(item);
}
ToggleChangeMy(item:Task){
  this.db.object("/Users/"+this.user.Id+"/Task/"+item.Id+"/IsDone").set(item.IsDone);
  console.log("/Users/"+this.user.Id+"/Task/"+item.Id+"/IsDone");
}
ToggleChangeTeam(item:Task){
  this.db.object("/Teams/"+this.user.TeamId+"/Tasks/Normal/"+item.Id+"/IsDone").set(item.IsDone);
  console.log("/Teams/"+this.user.TeamId+"/Tasks/Normal/"+item.Id+"/IsDone");

}
ToggleChangeTeamUrg(item:Task){
  this.db.object("/Teams/"+this.user.TeamId+"/Tasks/Urgent/"+item.Id+"/IsDone").set(item.IsDone);
}
public ArchiveTeamTask(item:Task){
this.db.object("/Teams/"+this.user.TeamId+"/Archive/"+item.Id).set(item);
this.TeamTasks.remove(item.Id);
}
public ArchiveUrgentTask(item:Task){
this.db.object("/Teams/"+this.user.TeamId+"/Archive/"+item.Id).set(item);
this.UrgentTasks.remove(item.Id);
}
opencreate(){
  this.navCtrl.push(TaskCreationPage,{
    id:this.auth.getUser().uid,
    username:this.user.NickName,
    teamid:this.teamId
  });
}

}
