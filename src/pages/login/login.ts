import { RegisterPage } from './../register/register';
import { HomePage } from './../home/home';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Loading,AlertController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'login',
  templateUrl: 'login.html',
})
export class LoginPage {
 loading: Loading;
  registerCredentials = { email: '', password: '' };
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private auth: AuthServiceProvider, 
  private alertCtrl: AlertController, 
  private loadingCtrl: LoadingController) {
    
  }
  ionViewWillEnter(){
 if (this.auth.authenticated) {        
        this.navCtrl.setRoot(HomePage);
        
      }
  }

  ionViewDidLoad() {
    if (this.auth.authenticated) {        
        this.navCtrl.setRoot(HomePage);
        
      }
  }
public login() {
    this.showLoading()
    this.auth.loginUser(this.registerCredentials.email,this.registerCredentials.password).then(allowed => {
      if (this.auth.authenticated) {   
             
        this.navCtrl.setRoot(HomePage);
        
      } else {
        this.showError("Access Denied");
      }
    },
      error => {
        this.showError(error);
      });
  }
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
   showError(text) {
   if(this.loading)
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
 LoadRegister(){
   this.navCtrl.push(RegisterPage);
 }
}
