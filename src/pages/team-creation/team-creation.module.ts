import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamCreationPage } from './team-creation';

@NgModule({
  declarations: [
    TeamCreationPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamCreationPage),
  ],
  exports: [
    TeamCreationPage
  ]
})
export class TeamCreationPageModule {}
