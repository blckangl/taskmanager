
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Team } from './../../models/Team';
import { User } from './../../models/User';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,LoadingController,Loading } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable  } from 'angularfire2/database';
import * as Clip from 'clipboard/dist/clipboard.min.js';

/**
 * Generated class for the TeamCreationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-team-creation',
  templateUrl: 'team-creation.html',
})
export class TeamCreationPage {
private user:User;
private team:Team;
private name:string;
private teamExist = false;
private teamMembers :string[];
private JoinId:string;
/////
itemUser: FirebaseObjectObservable<any>;
itemTeam: FirebaseObjectObservable<any>;

private text:string;
 loading: Loading;
/////
  constructor(public navCtrl: NavController, public navParams: NavParams,
   private db: AngularFireDatabase,
   private auth: AuthServiceProvider,
    public toastCtrl: ToastController,
    private loadingCtrl: LoadingController) {
     
     this.itemUser = db.object("/Users/"+this.auth.getUser().uid,{preserveSnapshot:true});
     this.itemUser.subscribe(snapshots=>{
       this.user=snapshots.val();
     })
this.team=new Team();
    this.text="";
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamCreationPage');
  }
createTeam(){
  
 if(this.text.length>4)
 {
   this.showLoading();
   this.team.Name=this.text;
   this.team.UserId.push(this.user.Id);
   this.team.Creator = this.user.Id;
let id = this.db.list("/Teams").push(this.team).key;
this.team.Id = id;
this.user.TeamId=id;
this.db.object("/Teams/"+id).update(this.team);
this.db.object("/Users/"+this.auth.getUser().uid+"/TeamId").set(id);
this.navCtrl.pop();

 }
 else{

   this.showMsg("team name is too short");
 }


}

joinTeamById(){
 
}

 showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
showMsg(msg:string) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }
}
