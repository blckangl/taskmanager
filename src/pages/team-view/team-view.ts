import { Chat } from './../../models/chat';
import { TeamJoinPage } from './../team-join/team-join';
import { TeamCreationPage } from './../team-creation/team-creation';
import { Team } from './../../models/Team';
import { User } from './../../models/User';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';

import * as Clip from 'clipboard/dist/clipboard.min.js';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
/**
 * Generated class for the TeamViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-team-view',
  templateUrl: 'team-view.html',
})
export class TeamViewPage {
userObserve : FirebaseObjectObservable<any>;
teamObserve : FirebaseObjectObservable<any>;
chatObserve : FirebaseListObservable<any>;
private teamMembers :string[];
private team:Team;
haveTeam = false;
clipboard:any;
private message:string;
username:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
   private db: AngularFireDatabase,
   private auth: AuthServiceProvider,
   public toastCtrl:ToastController) {
     
this.userObserve = this.db.object('/Users/'+this.auth.getUser().uid,{preserveSnapshot:true});
this.userObserve.subscribe(snapshots=>{
   this.username = snapshots.val()["NickName"];
  if(snapshots.val()["TeamId"]){
    console.log("***TeamId:"+snapshots.val()["TeamId"]);
    this.teamObserve = this.db.object('/Teams/'+snapshots.val()["TeamId"],{preserveSnapshot:true});
    this.teamObserve.subscribe(snapshot=>{
      if(snapshot.val()){    
         console.log("Team:"+snapshot.val());   
       this.team = new Team();
       this.team = snapshot.val();
       this.haveTeam = true;
       this.getTeamMembers();
       this.chatObserve = this.db.list("Chat/"+this.team.Id,{query: {
    limitToLast: 100
  }});
      }
      else{
   this.haveTeam = false;
     this.team = null;
      }
    })
  }
  else{
    this.team = null;
    this.haveTeam = false;
  }
})
 this.clipboard = new Clip('#cpyBtn');
        this.clipboard.on('success', () => this.showMsg(toastCtrl));
        this.message="";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeamViewPage');
  }
getTeamMembers(){
   this.teamMembers = [];
   if(this.team.UserId)
  this.team.UserId.forEach(id=>{
    this.db.object("/Users/"+id+"/NickName",{preserveSnapshot:true}).subscribe(snapshots=>{
      let temp = snapshots.val();
      console.log("**userrrr name = "+temp);
      this.teamMembers.push(temp);
    })
  })
}
showMsg(toastCtrl: ToastController) {
        let toast = toastCtrl.create({
            message: 'Id copied to clipboard',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    public openCreate(){
      this.navCtrl.push(TeamCreationPage);
    }
    public openJoin(){
      this.navCtrl.push(TeamJoinPage);
    }

    AddChat(){
      console.log(this.message)
            if(this.message.length>0 ){
              let chat = new Chat();
              chat.content=this.message;
              chat.date = new Date();
              chat.senderId =this.auth.getUser().uid;
              chat.senderName= this.username;
            
    chat.id =  this.chatObserve.push(chat).key;
    this.db.object("Chat/"+this.team.Id+"/"+chat.id).update(chat).then(success=>{
      this.message="";
    });

            }
    }
}
