import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamViewPage } from './team-view';

@NgModule({
  declarations: [
    TeamViewPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamViewPage),
  ],
  exports: [
    TeamViewPage
  ]
})
export class TeamViewPageModule {}
